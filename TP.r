rec <- read.table("recorridos1.csv", header = TRUE, sep = ",")
usu <- read.table("usuarios1.csv", header = TRUE, sep = ",")
edad <- table(usu$edad_usuario)
plot(edad, ylab = "cant", xlab = "edad")
usu
summary(usu$edad_usuario)
boxplot(usu$edad_usuario, horizontal = T, xlab = "Edad")
summary(rec$distancia)
boxplot(rec$distancia)


origen <- table(rec$direccion_estacion_origen)
top_origen <- sort(origen, decreasing = T)[0:10] #10 estaciones mas usadas
barplot(top_origen,las=2, cex.names = 0.5, col = "red",
        main = "Las 10 estaciones mas usadas")



destino <- table(rec$direccion_estacion_destino)
top_destino <- sort(destino, decreasing = T)[0:10]
barplot(top_destino, las=2, cex.names = 0.5, col = "red",
        main = "Las 10 estaciones mas usadas") #10 estaciones mas usadas
table(rec$id_usuario)

ori_dest <- top_origen[names(top_destino)]
dest_ori <- top_destino[names(top_origen)]

barplot(table(top_origen, dest_ori))
barplot(rbind(top_origen, dest_ori), col = c("#1b98e0", "#353436"), beside=T)
legend("topright",                                    # Add legend to barplot
       legend = c("Origen", "Destino"),
       fill = c("#1b98e0", "#353436"))

estaciones <- union(rec$direccion_estacion_destino, rec$direccion_estacion_origen)
diferencia_uso <- function(estacion) {
  if (!is.na(origen[estacion])) {
    if (!is.na(destino[estacion]))
      return (destino[estacion] - origen[estacion])
    else
      return (-origen[estacion])
  }
  else {
    if (!is.na(destino[estacion]))
      return (destino[estacion])
    else
      return (0)
  }
}


diferencia_uso_estaciones <- sapply(estaciones, diferencia_uso, USE.NAMES=F) # diferencia de bicicletas en la estacion
top_estaciones <- sort(diferencia_uso_estaciones, decreasing=T)[1:10] # mas bicicletas en la estacion
bottom_estaciones <- sort(diferencia_uso_estaciones)[1:10] # menos bicicletas en la estacion
top_estaciones <- top_estaciones[order(origen[names(top_estaciones)], decreasing = T)]
bottom_estaciones <- bottom_estaciones[order(origen[names(bottom_estaciones)], decreasing = T)]

# origen y destino (mas bicicletas)
par(mar=c(12,4,4,1))
barplot(rbind(origen[names(top_estaciones)], destino[names(top_estaciones)]),
        beside=T, las=2, cex.names = 0.7, col = c("#1b98e0", "#353436"),
        main="Mas bicicletas en la estacion") # diferencias mas grandes
legend("topright",                                    # Add legend to barplot
       legend = c("Origen", "Destino"),
       fill = c("#1b98e0", "#353436"))

# origen y destino (menos bicicletas)
par(mar=c(12,4,4,1))
barplot(rbind(origen[names(bottom_estaciones)], destino[names(bottom_estaciones)]),
        beside=T, las=2, cex.names = 0.7, col = c("#1b98e0", "#353436"),
        main="Menos bicicletas en la estacion") # diferencias mas grandes
legend("topright",                                    # Add legend to barplot
       legend = c("Origen", "Destino"),
       fill = c("#1b98e0", "#353436"))

plot(sort(origen))

usos <- cbind(table(rec$direccion_estacion_destino), table(rec$direccion_estacion_origen))
usos <- cbind(rec$direccion_estacion_destino, rec$direccion_estacion_origen)

summary(rec$duracion_recorrido)
table(rec$dia)
hist(rec$distancia)
hist(rec$duracion_recorrido)

excedido <- c(rec$id_usuario, rec$duracion_recorrido)[rec$duracion_recorrido > 1800]
excedido

usu_rec <- merge(usu, rec)
genero_viaje <- table(usu_rec$genero_usuario) # tabla de genero por los viajes
barplot(genero_viaje, col="#4F9E43", ylab="Viajes", axes=F)
axis(2, at=trunc(seq(from=0, to=max(genero_viaje), length.out=6)))

genero <- usu_rec$genero_usuario
distancia <- usu_rec$distancia
intervalos <- intervalos <- seq(from=min(distancia), to=max(distancia)+1000, by=1000)
genero_dist <- table(genero, cut(distancia, breaks=intervalos, include.lowest=T))
barplot(genero_dist, beside=T, col=c("red", "green", "blue"), xaxt="n", 
        xlab="Distancia", ylab="Usuarios")
legend("topright",                                    # Add legend to barplot
       legend = c("Mujeres", "Hombres", "Otros"),
       fill = c("red", "green", "blue"))

marcas <- cbind(
  seq(from=0, to=max(distancia)+1000, length.out=5),
  seq(from=1, to=dim(genero_dist)[2]*4, length.out=5))
ag_marca <- function(marca) {
  marca <- trunc(marca)
  mtext(marca[1], side=1, line=0, at=marca[2])
}
apply(marcas, 1, ag_marca)


